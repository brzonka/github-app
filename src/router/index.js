import Vue from 'vue'
import Router from 'vue-router'
import Repositories from '@/components/Repositories'
import Welcome from '@/components/Welcome'
import Activity from '@/components/Activity'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'welcome',
      component: Welcome
    },
    {
      path: '/repositories',
      name: 'repositiories',
      component: Repositories
    },
    {
      path: '/activity',
      name: 'activity',
      component: Activity
    }
  ],
  mode: 'history'
})
