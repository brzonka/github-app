import Vue from 'vue';
export default {
  getRepositiories(store, name){
    Vue.http.get(`${store.state.API}/${name}/repos`).then((res) => {
      store.state.repositories = res.data;
    });
  },
  getActivity(store, name){
    Vue.http.get(`${store.state.API}/${name}/events`).then((res) => {
      store.state.activity = res.data;
    });
  }
}
