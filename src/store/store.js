import Vue from 'vue';
import Vuex from 'vuex';
import Vueres from 'vue-resource';

Vue.use(Vuex);
Vue.use(Vueres);

import getters from './getters';
import mutations from './mutations';
import actions from './actions';

export const store = new Vuex.Store({
  state: {
    user: '',
    repositories: [],
    activity: [],
    API: 'http://api.github.com/users'
  },
  getters,
  mutations,
  actions
});
